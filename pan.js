function pan (el,opts) {
	el.addClass('pan')
	el.children(0).draggable({
		drag:function (ev,ui) {
			var $t=$(this),
				$p=$t.parent(),
				dsize={
					width:$p.width()-$t.width(),
					height:$p.height()-$t.height()},
				pos_bot={
					bottom:dsize.height-ui.position.top,
					right:dsize.width-ui.position.left};
			if (dsize.width>0)	
				ui.position.left = dsize.width/2.0;
			else if(ui.position.left > 0)	
				ui.position.left = 0;
			else if(pos_bot.right > 0)	
				ui.position.left = dsize.width;
			if (dsize.height>0)	
				ui.position.top = dsize.height/2.0;
			else if(ui.position.top > 0)	
				ui.position.top = 0;
			else if(pos_bot.bottom > 0)	
				ui.position.top = dsize.height;
		}
	})
}